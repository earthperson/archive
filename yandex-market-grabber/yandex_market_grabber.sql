-- phpMyAdmin SQL Dump
-- version 2.11.9.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 19, 2009 at 05:23 PM
-- Server version: 5.0.45
-- PHP Version: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yandex_market_grabber`
--

-- --------------------------------------------------------

--
-- Table structure for table `gr_prod`
--

CREATE TABLE IF NOT EXISTS `gr_prod` (
  `prod_id` int(11) NOT NULL auto_increment,
  `project_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `market_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `min_price` decimal(12,2) NOT NULL,
  `avg_price` decimal(12,2) NOT NULL,
  `max_price` decimal(12,2) NOT NULL,
  `capture_time` datetime NOT NULL,
  `capture_mode` varchar(255) NOT NULL,
  PRIMARY KEY  (`prod_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `gr_project`
--

CREATE TABLE IF NOT EXISTS `gr_project` (
  `project_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`project_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `gr_properties`
--

CREATE TABLE IF NOT EXISTS `gr_properties` (
  `id` int(11) NOT NULL auto_increment,
  `prod_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `prod_id` (`prod_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
