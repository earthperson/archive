-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 03, 2009 at 08:34 AM
-- Server version: 5.0.88
-- PHP Version: 5.2.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `filemanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `journal`
--

DROP TABLE IF EXISTS `journal`;
CREATE TABLE IF NOT EXISTS `journal` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `file` varchar(32) NOT NULL,
  `type` enum('file','folder') NOT NULL default 'file',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `file` (`file`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `journal`
--

INSERT INTO `journal` (`id`, `parent_id`, `file`, `type`, `name`) VALUES
(63, 0, 'abd95ae4e98fe230d2f0ec2f1d2ff04a', 'folder', 'Первая папка'),
(64, 63, '1393cdf32a953413d883b9a75f41fcae', 'folder', 'В ней еще'),
(65, 64, '2ce24bfbb570eda58f28f3a8a66c927e', 'folder', 'И еще'),
(66, 0, '201b50ddfce75ec7fae7f25a9a5004f4', 'folder', 'Node 2'),
(67, 66, '17c182f9840427b56310dda0f8bda1b1', 'folder', 'Child node'),
(68, 66, 'a92d1f1545a6f24d487fda65a74395b8', 'file', 'ZendFramework1.6.pdf');
