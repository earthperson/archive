-- phpMyAdmin SQL Dump
-- version 2.11.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 09, 2010 at 05:02 PM
-- Server version: 5.1.48
-- PHP Version: 5.3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smsgateway`
--

-- --------------------------------------------------------

--
-- Table structure for table `modems`
--

DROP TABLE IF EXISTS `modems`;
CREATE TABLE IF NOT EXISTS `modems` (
  `modem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `note` text  CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
  `balance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sms_package` int(11) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`modem_id`),
  UNIQUE KEY `host` (`host`,`port`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `modems`
--

INSERT INTO `modems` (`modem_id`, `host`, `port`, `note`, `balance`, `sms_package`, `locked`, `disabled`, `modified`, `created`) VALUES
(1, 'ponomarev@calf', '/dev/ttyS0', '', '', 0, 0, 0, '2010-07-08 16:55:42', '0000-00-00 00:00:00'),
(2, 'ponomarev@calf', '/dev/ttyS1', '', '', 0, 0, 1, '2010-07-08 16:55:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

DROP TABLE IF EXISTS `sms`;
CREATE TABLE IF NOT EXISTS `sms` (
  `sms_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `da` varchar(255)  CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '',
  `text` text  CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
  `modem_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`sms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sms`
--


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255)  CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '',
  `email` varchar(255)  CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '',
  `password` varchar(255)  CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '',
  `note` text  CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `note`, `modified`, `created`) VALUES
(1, '', 'ponomarev.base@gmail.com', '202cb962ac59075b964b07152d234b70', 'debugger', '2010-07-06 11:56:34', '0000-00-00 00:00:00');
