--
-- Database: `forum`
--
CREATE DATABASE `forum` DEFAULT CHARACTER SET cp1250 COLLATE cp1250_general_ci;
USE `forum`;

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `login` char(10) character set cp1250 collate cp1250_bin NOT NULL default '',
  `passwd` char(10) character set cp1250 collate cp1250_bin NOT NULL default '',
  `name` varchar(255) character set cp1250 collate cp1250_bin NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1250 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `created`, `login`, `passwd`, `name`) VALUES
(1, '2010-01-09 03:49:03', 'dimalogin', '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `parent` int(11) unsigned default NULL,
  `title` varchar(255) NOT NULL default '',
  `poster` varchar(255) NOT NULL default '',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `body` blob,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1250 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id`, `parent`, `title`, `poster`, `created`, `body`) VALUES
(1, 1, 'asdf', '123', '2010-01-09 03:52:03', 0x61736466),
(2, 1, 'fasd', '123', '2010-01-09 03:52:21', 0x66736466);

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE IF NOT EXISTS `response` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `parent` int(11) unsigned default NULL,
  `poster` varchar(255) NOT NULL default '',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `body` blob,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1250 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `response`
--

INSERT INTO `response` (`id`, `parent`, `poster`, `created`, `body`) VALUES
(1, 2, '123', '2010-01-09 03:52:27', 0x6661736466),
(2, 2, '123', '2010-01-09 03:52:29', 0x646661736466);

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `poster` varchar(255) NOT NULL default '',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1250 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `title`, `poster`, `created`) VALUES
(1, 'asfs', '123', '2010-01-09 03:52:03');
